﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClueController : MonoBehaviour
{
  public GameObject saxObject;
  public GameObject photoObject;
  public GameObject purseObject;

  public GameObject saxParent;
  public GameObject photoParent;
  public GameObject purseParent;

  public bool saxActive = false;
  public bool photoActive = false;
  public bool purseActive = false;

  public bool saxFound = false;
  public bool photoFound = false;
  public bool purseFound = false;

  void Update()
  {
    if(!globalScript.instance.talkedToCharacter)
    {
      saxObject.SetActive(false);
      photoObject.SetActive(false);
      purseObject.SetActive(false);
    } else if (globalScript.instance.talkedToCharacter) {
      saxParent.SetActive(true);
      photoParent.SetActive(true);
      purseParent.SetActive(true);
    }

    if (saxFound)
    {
      globalScript.instance.saxFound = true;
    }
    if (photoFound)
    {
      globalScript.instance.photoFound = true;
    }
    if (purseFound)
    {
      globalScript.instance.purseFound = true;
    }
  }
}
