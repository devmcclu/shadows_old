﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI;
    public GameObject resume;
    public GameObject howToPlay;
    public GameObject quitGame;

    public GameObject rulesUI;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
          if (GameIsPaused)
          {
            Resume();
          } else
          {
            Pause();
          }
        }
    }

    public void Resume()
    {
      pauseMenuUI.SetActive(false);
      Time.timeScale = 1f;
      GameIsPaused = false;
    }

    public void HowToPlay()
    {
      resume.SetActive(false);
      howToPlay.SetActive(false);
      quitGame.SetActive(false);

      rulesUI.SetActive(true);
    }

    public void Back()
    {
      resume.SetActive(true);
      howToPlay.SetActive(true);
      quitGame.SetActive(true);

      rulesUI.SetActive(false);
    }

    void Pause()
    {
      pauseMenuUI.SetActive(true);
      Time.timeScale = 0f;
      GameIsPaused = true;
    }

    public void QuitGame()
    {
      Application.Quit();
    }

}
