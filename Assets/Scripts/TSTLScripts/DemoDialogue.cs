﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DemoDialogue : MonoBehaviour
{
  public GameObject conversation;
  public GameObject shadow;
  public GameObject character;
  public GameObject dialogueUI;

  public GameObject characterBubble;
  public GameObject shadowBubble;
  public GameObject greenGlow;
  public GameObject crystalBall;

  public Text narration;
  public Text dialogue;
  public Text shadowDialogue;

  public GameObject questions;
  public GameObject noTelling;
  public GameObject angryQuestion;
  public GameObject notAngry;
  public GameObject noWorry;
  public GameObject whySorry;
  public GameObject channelCrystalBall;
  public GameObject channelCrystalBall1;
  public GameObject whatDoYouMean;
  public GameObject whatSheDid;
  public GameObject elaborate;
  public GameObject notBad;
  public GameObject shadowFades;
  public GameObject shadowFades1;
  public GameObject relationshipVivian;
  public GameObject whyHate;
  public GameObject hurtHer;
  public GameObject tellMeMore;
  public GameObject fullTruth;
  public GameObject continueButton1;
  public GameObject continueButton2;
  public GameObject continueButton3;
  public GameObject continueButton01;
  public GameObject continueButton02;
  public GameObject continueButton10;
  public GameObject continueButton20;
  public GameObject explore;

  public void Questions()
  {
    greenGlow.SetActive(false);
    questions.SetActive(false);  //disables options when choice taken
    noTelling.SetActive(false);
    narration.text = "Lorraine's shoulders drop slightly.";
    dialogue.text = "Oh, thank goodness. She must be angry.";
    angryQuestion.SetActive(true);
    notAngry.SetActive(true);
  }

  public void WhySorry()
  {
    greenGlow.SetActive(true);
    whySorry.SetActive(false);
    noWorry.SetActive(false);
    narration.text = "Lorraine holds her hands up to her neck. Your crystal ball begins glowing. Light fills your eyes but Lorraine doesn't seem to notice, as her eyes shift to different places in the room behind you.";
    dialogue.text = "Umm... we didn't have a good relationship. That's all.";
    elaborate.SetActive(true);
    channelCrystalBall1.SetActive(true);
    notBad.SetActive(true);
  }

  public void NotBad()
  {
    greenGlow.SetActive(false);
    elaborate.SetActive(false);  //disables options when choice taken
    channelCrystalBall.SetActive(false);
    notBad.SetActive(false);
    fullTruth.SetActive(false);
    narration.text = "Lorraine's shoulders drop slightly.";
    dialogue.text = "Oh, thank goodness. She must be angry.";
    angryQuestion.SetActive(true);
    notAngry.SetActive(true);
  }

  public void Elaborate()
  {
    elaborate.SetActive(false);  //disables options when choice taken
    channelCrystalBall.SetActive(false);
    notBad.SetActive(false);
    narration.text = "Lorraine slouches a bit and she looks at the floor. Your crystal ball glows a faint green. You feel something else in the room with you and Lorraine.";
    dialogue.text = "Well, we weren't on the best of terms when she died. I just wish we could have made up before she left.";
    greenGlow.SetActive(true);
    channelCrystalBall.SetActive(true);
    whatDoYouMean.SetActive(true);
  }

  public void NoTelling()
  {

    greenGlow.SetActive(false);
    questions.SetActive(false);  //disables options when choice taken
    noTelling.SetActive(false);
    narration.text = "Lorraine jumps back a bit.";
    dialogue.text = "She's still here? Please go away Vivian. I'm sorry!";
    noWorry.SetActive(true);
    whySorry.SetActive(true);
  }

  public void NoWorry()
  {

    noWorry.SetActive(false);
    whySorry.SetActive(false);
    narration.text = "Lorraine's shoulders drop slightly.";
    dialogue.text = "Oh, thank goodness. She must be angry.";
    angryQuestion.SetActive(true);
    notAngry.SetActive(true);
  }

  public void NotAngry()
  {
    angryQuestion.SetActive(false);
    notAngry.SetActive(false);
    narration.text = "Lorraine slouches a bit and she looks at the floor. Your crystal ball glows a faint green. You feel something else in the room with you and Lorraine";
    dialogue.text = "Well, we weren't on the best of terms when she died. I just wish we could have made up before she left.";
    greenGlow.SetActive(true);
    channelCrystalBall.SetActive(true);

    whatDoYouMean.SetActive(true);
  }

  public void AngryQuestion()
  {
    angryQuestion.SetActive(false);
    notAngry.SetActive(false);
    narration.text = "Lorraine slouches a bit and she looks at the floor. Your crystal ball glows a faint green. You feel something else in the room with you and Lorraine";
    dialogue.text = "Well, we weren't on the best of terms when she died. I just wish we could have made up before she left.";
    greenGlow.SetActive(true);
    channelCrystalBall.SetActive(true);

    whatDoYouMean.SetActive(true);
  }

  public void ChannelCrystalBall()
  {

    crystalBall.SetActive(true);
    greenGlow.SetActive(true);

    channelCrystalBall.SetActive(false);
    whatDoYouMean.SetActive(false);
    character.SetActive(false);
    characterBubble.SetActive(false);
    shadow.SetActive(true);
    shadowBubble.SetActive(true);
    narration.text = "A shadow manifests behind Vivian. Several deformed arms, each of differing length and shape, emerge from its torso. A faint black mist is swirling around it. And it emits a dark green glow.";
    shadowDialogue.text = "Do not risk talking with us. Love is an awfully powerful force that's all we have left.";
    shadowFades.SetActive(true);
  }

  public void ChannelCrystalBall1()
  {

    crystalBall.SetActive(true);
    greenGlow.SetActive(true);

    channelCrystalBall1.SetActive(false);
    elaborate.SetActive(false);
    notBad.SetActive(false);
    characterBubble.SetActive(false);
    shadow.SetActive(true);
    shadowBubble.SetActive(true);
    narration.text = "A dark figure appears behind Lorraine. It has several arms jutting out from it's torso. Each one is contorted in a different way. Time seems to have stopped around you.";
    shadowDialogue.text = "You should not be talking to us. You do not know what we could do to you if you get in our way.";
    shadowFades1.SetActive(true);
  }

  public void ShadowFades1()
  {

    crystalBall.SetActive(true);
    greenGlow.SetActive(false);
    shadowFades1.SetActive(false);
    shadow.SetActive(false);
    shadowBubble.SetActive(false);
    character.SetActive(true);
    characterBubble.SetActive(true);
    narration.text = "Lorraine holds her hands up to her neck.";
    dialogue.text = "Umm... we didn't have a good relationship. That's all.";
    fullTruth.SetActive(true);
    elaborate.SetActive(true);
    notBad.SetActive(true);
  }

  public void FullTruth()
  {

    greenGlow.SetActive(false);
    fullTruth.SetActive(false);
    elaborate.SetActive(false);
    notBad.SetActive(false);
    narration.text = "Lorraine is startled by your question.";
    dialogue.text = "Ah. You're right. I hated Vivian. Why did she get to marry Damon, while I had to sit back and watch?";
    tellMeMore.SetActive(true);
  }

  public void TellMeMore()
  {

    greenGlow.SetActive(false);
    tellMeMore.SetActive(false);
    narration.text = "Lorraine's eyes narrow and she raises an eyebrow.";
    dialogue.text = "I just wish she hadn't taken him from me. What did she have that I don't?";
    continueButton10.SetActive(true);
  }

  public void ContinueButton10()
  {

    greenGlow.SetActive(false);
    continueButton10.SetActive(false);
    narration.text = "";
    dialogue.text = "Wait. You don't think I killed her do you? I couldn't do that.";
    continueButton20.SetActive(true);
  }

  public void ContinueButton20()
  {

    greenGlow.SetActive(false);
    continueButton20.SetActive(false);
    narration.text = "";
    dialogue.text = "Leave. I didn't have anything to do with this. Go find some real evidence.";
    explore.SetActive(true);
  }

  public void ShadowFades()
  {

    crystalBall.SetActive(true);
    greenGlow.SetActive(false);
    shadowFades.SetActive(false);
    shadow.SetActive(false);
    shadowBubble.SetActive(false);
    character.SetActive(true);
    characterBubble.SetActive(true);
    narration.text = "Lorraine slouches a bit and she looks at the floor.";
    dialogue.text = "Well, we weren't on the best of terms when she died. I just wish we could have made up before she left.";
    whatDoYouMean.SetActive(true);
    relationshipVivian.SetActive(true);
  }

  public void RelationshipVivian()
  {

    greenGlow.SetActive(false);
    whatDoYouMean.SetActive(false);
    relationshipVivian.SetActive(false);
    narration.text = "Lorraine tenses up. Her eyes dart around the room searching for anything to look at except you.";
    dialogue.text = "I hated her. Is that what you wanted me to tell you?";
    whyHate.SetActive(true);
  }

  public void WhyHate()
  {

    greenGlow.SetActive(false);
    whyHate.SetActive(false);
    narration.text = "Lorraine sighs. Her arms fold around her as if to hug herself.";
    dialogue.text = "Because she married Damon. Why did she have to take him from me?";
    hurtHer.SetActive(true);
  }

  public void WhatDoYouMean()
  {

    greenGlow.SetActive(false);
    whatDoYouMean.SetActive(false);
    narration.text = "Lorraine's eyes widen for a moment, surprised by your question.";
    dialogue.text = "She, um... She did something I really wish she hadn't.";
    whatSheDid.SetActive(true);
  }

  public void WhatSheDid()
  {

    greenGlow.SetActive(false);
    whatSheDid.SetActive(false);
    relationshipVivian.SetActive(false);
    narration.text = "Lorraine hesitates answering. After a few moments, she speaks.";
    dialogue.text = "I don't feel comfortable answering that. Not if she could hear me.";
    continueButton01.SetActive(true);
  }

  public void ContinueButton01()
  {

    greenGlow.SetActive(false);
    continueButton01.SetActive(false);
    narration.text = "";
    dialogue.text = "If you really want to know more, I'm sure you can find something around the house that can point you in the right direction.";
    continueButton02.SetActive(true);
  }

  public void ContinueButton02()
  {

    greenGlow.SetActive(false);
    continueButton02.SetActive(false);
    narration.text = "";
    dialogue.text = "You might look around in the library. I know Vivian spent a lot of time in there.";
    explore.SetActive(true);
  }

  public void HurtHer()
  {

    greenGlow.SetActive(false);
    hurtHer.SetActive(false);
    narration.text = "Lorraine's expression grows shocked.";
    dialogue.text = "What? No! I would never.";
    continueButton1.SetActive(true);
  }

  public void ContinueButton1()
  {

    greenGlow.SetActive(false);
    continueButton1.SetActive(false);
    narration.text = "";
    dialogue.text = "I'm sorry, I don't want to talk about this anymore.";
    continueButton2.SetActive(true);
  }

  public void ContinueButton2()
  {

    greenGlow.SetActive(false);
    continueButton2.SetActive(false);
    narration.text = "";
    dialogue.text = "Go look for something else. There must be something around like a clue.";
    continueButton3.SetActive(true);
  }

  public void ContinueButton3()
  {

    greenGlow.SetActive(false);
    continueButton3.SetActive(false);
    narration.text = "";
    dialogue.text = "If you really want to find who did this, you're wasting your time talking to me.";
    explore.SetActive(true);
  }

  public void Explore()
  {

    greenGlow.SetActive(false);
    explore.SetActive(false);
    dialogueUI.SetActive(false);
    globalScript.instance.talkedToCharacter = true;
    globalScript.instance.characterClicked = false;
  }

  void Update()
  {
    if (globalScript.instance.talkedToCharacter && globalScript.instance.characterClicked)
    {
      dialogueUI.SetActive(true);
      narration.text = "";
      dialogue.text = "You still need to find more clues. Try the library.";
      explore.SetActive(true);
    }
  }
}
