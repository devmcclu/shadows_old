﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class endMenu : MonoBehaviour
{
  public GameObject newGame;
  public GameObject quitGame;

  public string newGameScene;

  public void NewGame()
  {
    SceneManager.LoadScene(newGameScene);
  }

  public void QuitGame()
  {
    Application.Quit();
  }
}
