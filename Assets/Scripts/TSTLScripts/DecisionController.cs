﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DecisionController : MonoBehaviour
{
  public Decision decision;
  public Button choiceButton;

  private List<ChoiceController> choiceControllers = new List<ChoiceController>();

  public void Change(Decision _decision)
  {
    RemoveChoices();
    decision = _decision;
    gameObject.SetActive(true);
    Initialize();
  }

  public void Hide(Conversation conversation)
  {
    RemoveChoices();
    gameObject.SetActive(false);
  }

  private void RemoveChoices()
  {
    foreach(ChoiceController c in choiceControllers)
    {
      Destroy(c.gameObject);
    }

    choiceControllers.Clear();
  }

  private void Start() {}

  private void Initialize()
  {
    for(int index = 0; index < decision.choices.Length; index++)
    {
      ChoiceController c = ChoiceController.AddChoiceButton(choiceButton, decision.choices[index], index);
      choiceControllers.Add(c);
    }

    choiceButton.gameObject.SetActive(false);
  }
}
