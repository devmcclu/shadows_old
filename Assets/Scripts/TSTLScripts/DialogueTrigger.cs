﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
  public GameObject dialogueUI;

  void Update()
  {
    if (globalScript.instance.characterClicked && !globalScript.instance.talkedToCharacter)
    {
      dialogueUI.SetActive(true);
    }
  }

  void OnMouseDown()
  {
    globalScript.instance.characterClicked = true;
  }
}
