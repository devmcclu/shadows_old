﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndController : MonoBehaviour
{
  public GameObject endUI;

  public GameObject Fader;

  private Animator anim;

  public string newGameScene;

  void Awake ()
  {
    anim = Fader.GetComponent<Animator>();
  }

  void FixedUpdate()
  {
    if (globalScript.instance.saxFound && globalScript.instance.photoFound && globalScript.instance.purseFound)
    {
      FadeOut();
      Invoke("ActivateEndUI", 1.5f);
      Invoke("FadeIn", 1.5f);
    }
  }

  public void NewGame()
  {
    SceneManager.LoadScene(newGameScene);
  }

  public void QuitGame()
  {
    Application.Quit();
  }

  void ActivateEndUI()
  {
    endUI.SetActive(true);
  }

  void FadeOut()
  {
    anim.SetBool("FadeI", false);
    anim.SetBool("FadeO", true);
  }

  void FadeIn()
  {
    anim.SetBool("FadeO", false);
    anim.SetBool("FadeI", true);
  }
}
