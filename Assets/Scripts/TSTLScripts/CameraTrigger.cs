using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraTrigger : MonoBehaviour {

  public Transform roomCamera;

  public Camera camera;

  void Update()
  {
    transform.position = Vector3.Lerp(transform.position, roomCamera.position, Time.deltaTime);

  }


}
