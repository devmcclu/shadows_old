﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cyrstalBall : MonoBehaviour
{

  public GameObject item;
  public GameObject blackItem;

  public bool playerByObject;
  public bool spacePressed;

  void Update()
  {
    if (Input.GetKeyDown(KeyCode.Space))
    {
      spacePressed = true;
    }

    if(spacePressed && playerByObject)
    {
      item.SetActive(true);
      blackItem.SetActive(false);
    }

  }

  void OnTriggerEnter2D(Collider2D other)
    {
      if (other.CompareTag("Orb"))
      {
          playerByObject = true;
          print("Player by object");
      }

    }

    void OnTriggerExit2D(Collider2D other) //collision detection for player leaving doorway
    {
        if (other.CompareTag("Orb"))
        {
            playerByObject = false;
            spacePressed = false;
        }

    }
}
