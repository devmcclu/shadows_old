﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainMenu : MonoBehaviour
{
    public GameObject title;
    public GameObject newGame;
    public GameObject howToPlay;
    public GameObject quitGame;

    public GameObject rulesUI;

    public string newGameScene;

    public void NewGame()
    {
      SceneManager.LoadScene(newGameScene);
    }

    public void HowToPlay()
    {
      title.SetActive(false);
      newGame.SetActive(false);
      howToPlay.SetActive(false);
      quitGame.SetActive(false);

      rulesUI.SetActive(true);
    }

    public void Back()
    {
      title.SetActive(true);
      newGame.SetActive(true);
      howToPlay.SetActive(true);
      quitGame.SetActive(true);

      rulesUI.SetActive(false);
    }

    public void QuitGame()
    {
      Application.Quit();
    }
}
