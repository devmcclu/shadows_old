﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct Choice
{
  [TextArea(2, 5)]
  public string text;
  public Conversation conversation;
}

[CreateAssetMenu(fileName = "New Choice", menuName = "Choice")]
public class Decision : ScriptableObject
{
  [TextArea(2, 5)]
  public string text;
  public Choice[] choices;
}
