using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
  // Start is called before the first frame update
    void Start()
    {
        target = transform.position;
    }

    // Update is called once per frame
    public static float speed = 3.0f;//player movement speed
    bool playerMoving;//if player is moving
    bool clickedOnDoor;
    Vector3 target;//player goal to move to
    void Update()
    {
        if (Input.GetMouseButtonUp(0))//if game isnt paused and a location is clicked, this funciton moves the player to the location
        {
            if (playerMoving)
            {
                clickedOnDoor = false;
            }
            playerMoving = !playerMoving;
            target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                target.z = transform.position.z;
                target.y = transform.position.y;
        }
        if (playerMoving) {//this stops the player from moving if they were already
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target, step);
            if (target.x == transform.position.x)
                playerMoving = false;
        }
        if (Input.GetMouseButtonDown(1)) //this checks to see if the game is paused
        {
            playerMoving = false;
        }
    }
}


/*  public static float speed = 3.0f;

  public AudioClip AudioFile;

  void Update()
  {
    var move = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
    transform.position += move * speed * Time.deltaTime;

    if (Input.GetAxis("Horizontal") != 0){
      GetComponent<AudioSource>().UnPause();
      }
    else {
        GetComponent<AudioSource>().Pause();
      }
  }
}*/
