﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoController : MonoBehaviour
{

  public AudioClip pickUpSound;

  AudioSource audio;

  public bool alreadyPlayed;

  public GameObject itemText;

  public GameObject item;

  void Start()
  {
    audio = GetComponent<AudioSource>();
  }

  void OnMouseDown()
  {
    item.SetActive(true);
    itemText.SetActive(true);
    globalScript.instance.photoFound = true;
    if (!alreadyPlayed)
    {
      audio.PlayOneShot(pickUpSound);
      alreadyPlayed = true;
    }
  }

}
