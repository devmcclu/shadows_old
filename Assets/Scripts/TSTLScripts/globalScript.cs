using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class globalScript : MonoBehaviour
{
    public static globalScript instance;
    public bool talkedToCharacter = false;
    public bool characterClicked = false;
    public bool diningRoomExitClicked = false;
    public bool purseFound = false;
    public bool saxFound = false;
    public bool photoFound = false;

    void Start()
    {
      instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
