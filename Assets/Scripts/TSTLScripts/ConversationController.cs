using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[System.Serializable]
public class DecisionEvent : UnityEvent<Decision> {}

public class ConversationController : MonoBehaviour
{
  public Conversation conversation;
  public DecisionEvent decisionEvent;

  public GameObject speakerRight;
  public GameObject speakerLeft;

  private speakerUI speakerUIRight;
  private speakerUI speakerUILeft;

  private int activeLineIndex;
  private bool conversationStarted = false;

  public void ChangeConversation(Conversation nextConversation)
  {
    conversationStarted = false;
    conversation = nextConversation;
    AdvanceLine();
  }

  void Start()
  {
    speakerUILeft = speakerLeft.GetComponent<speakerUI>();
    speakerUIRight = speakerRight.GetComponent<speakerUI>();
  }

  void Update()
  {
    if (Input.GetKeyDown("space"))
    {
      AdvanceLine();
    } else if (Input.GetKeyDown("x")) {
      EndConversation();
    }
  }

  private void EndConversation()
  {
    conversation = null;
    conversationStarted = false;
    speakerUILeft.Hide();
    speakerUIRight.Hide();
  }

  private void Initialize()
  {
    conversationStarted = true;
    activeLineIndex = 0;
    speakerUILeft.Speaker = conversation.speakerLeft;
    speakerUIRight.Speaker = conversation.speakerRight;
  }

  private void AdvanceLine()
  {
    if (conversation == null) return;
    if (!conversationStarted) Initialize();

    if (activeLineIndex < conversation.lines.Length)
    {
      DisplayLine();
    } else {
      AdvanceConversation();
    }
  }

  private void DisplayLine()
  {
    Line line = conversation.lines[activeLineIndex];
    Character character = line.character;

    if (speakerUILeft.SpeakerIs(character))
    {
      SetDialogue(speakerUILeft, speakerUIRight, line.text);
    }
    else {
      SetDialogue(speakerUIRight, speakerUILeft, line.text);
    }

    activeLineIndex += 1;
  }

  private void AdvanceConversation()
  {
    if (conversation.decision != null)
    {
      decisionEvent.Invoke(conversation.decision);
    } else if (conversation.nextConversation != null) {
      ChangeConversation(conversation.nextConversation);
    } else {
      EndConversation();
    }
  }

  private void SetDialogue(speakerUI activeSpeakerUI, speakerUI inactiveSpeakerUI, string text)
  {
    activeSpeakerUI.Dialogue = text;
    activeSpeakerUI.Show();
    inactiveSpeakerUI.Hide();
  }
}
