
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class teleport : MonoBehaviour
{

    public Transform player;
    public Transform roomCamera;
    public Transform enterRoom;

    public bool playerInDoorway;
    public bool clickedOnDoor;

    public GameObject Fader;
    private Animator anim;

    void Awake ()
    {
      anim = Fader.GetComponent<Animator>();
    }

    void GoToDestination ()
    {
      Camera.main.transform.position = roomCamera.position;
    }

    void OnMouseDown()//sets global to know that player mousedowned on the door
    {
      FadeOut();
      Invoke("GoToDestination", 1.5f);
      Invoke("FadeIn", 1.5f);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
      if (other.CompareTag("Player"))
      {
          playerInDoorway = true;
          print("Player is in doorway right now");
      }

    }

    void OnTriggerExit2D(Collider2D other) //collision detection for player leaving doorway
    {
        if (other.CompareTag("Player"))
        {
            playerInDoorway = false;
            clickedOnDoor = false;
            print("Player is NOT in doorway right now");
        }

    }

    void FadeOut()
    {
      anim.SetBool("FadeI", false);
      anim.SetBool("FadeO", true);
    }

    void FadeIn()
    {
      anim.SetBool("FadeO", false);
      anim.SetBool("FadeI", true);
    }
}
